<?php
	class ProductList extends CFormModel
	{
		public $prd;
		private $n;
		
		public function initPrdList()
		{
			$sql = "SELECT * from sanpham ORDER BY MaSP";
			$rows = Yii::app()->db->createCommand($sql)->queryAll();
			
			if ($rows == NULL)
			{
				echo "Khong ton tai san pham nao, moi ban thu lai";
				$this -> prd = NULL;
			}
			else
			{                
                $this->n = 0;
                foreach ($rows as $row) {
                    if ($row['img_url'] == NULL)
                        $row['img_url'] = 'http://www.almargen.com/blog/wp-content/themes/barajador/img/no-pre.png';
                    $this->book[++$this->n] = $row;}
			}
		}
             
		public function getNumberPrd()
		{
			return $this -> n;
		}
		
		public static function getPrdbyId($id)
		{
			$sql = "SELECT * from sanpham WHERE MaSP = '$id' ORDER BY MaSP";
			$rows = Yii::app()->db->createCommand($sql)->queryAll();
			
			if ($rows == NULL)
			{
				echo "Khong ton tai san pham co ma san pham nhu tren";
			}
			else
			{
				return $rows;
			}
		}
		
		public static function getPrdbyCat($cat)
		{
			$sql = "SELECT * from sanpham WHERE MaLoaiSP = '$cat' ORDER BY MaSP";
			$rows = Yii::app()->db->createCommand($sql)->queryAll();
			
			if ($rows == NULL)
			{
				echo "Khong ton tai san pham";
			}
			else
			{
				return $rows;
			}
		}
			
	}
?>	
