<?php
	class RegisterForm extends CFormModel
	{
		public $username;
		public $password;
		public $mes;
		
		public function RegisterForm($username, $password)
		{
			$this->username = $username;
			$this->password = $password;
		}
		
		public function validate()
		{
			$username = $this->username;
			$password = $this->password;
			
			if ($username==null || $password==null)
			{
				$this->mes = "Vui long dien day du username va mat khau";
				return false;
			}
			
			$username = String::sanitize($username);
			$password = String::sanitize($password);
			
			$sql = "SELECT * FROM khachhang WHERE TaiKhoan = $username";
			$row = Yii::app()->db->createCommand($sql)->queryRow();
			if ($row)
			{
				$this->mes = "Ten tai khoan cua ban da bi trung";
				return false;
			}
			
			$this->mes = "Dang ky thanh cong";
			return true;
		}
		
		public function register()
		{
			$sql = "INSERT INTO user(TaiKhoan, MatKhau) VALUES('$this->username', '$this->password')";
            $row = Yii::app()->db->createCommand($sql)->execute();
		}
	}
?>