<?php
	class PushBookForm extends CFormModel
	{
		public $name;
		public $prdCode;
		public $catCode;
		public $price;
		public $stt;
		public $img_url;
		public $des;
		
		public function PushBookForm($name, $prdCode, $catCode, $price, $stt, $img_url, $des)
		{
			$this->catCode = $catCode;
			$this->name = $name;
			$this->prdCode=$prdCode;
			$this->price=$price;
			$this->stt=$stt;
			$this->des=$des;
			$this->img_url=$img_url;
		}
		
		public function validate()
		{
			if (!is_numeric($this->price)) return false;
        	if ($this->cate == ":: Vui long chon loai san pham ::") return false;                     
        	return true;
		}
		
		public function push()
		{
			$sql = "INSERT INTO sanpham() VALUES()";
			Yii::app()->db->createCommand($sql)->execute();
		}
	}
?>