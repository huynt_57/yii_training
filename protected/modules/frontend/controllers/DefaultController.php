<?php

class DefaultController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$prdList = new ProductList();
		$prdList -> initPrdList();
		
		$this->render('index', array(
            'prdList'=>$prdList
            )
        );
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	public function actionPushPrd()
	{
		$mes = "";
		$modelForm = new PushProductForm($_POST());
		
		if (!$modelForm -> validate()) $mes = "Cu phap ban nhap co loi, vui long nhap lai";
		else
		{
			$modelForm->push();
		}
		$this->render('pushPrd', array('mes'=>$mes));
	}
	
	/**
     * Del
     */
    public function actionDel() {
        $id = $_GET['id'];
        
        $sql = "DELETE FROM sanpham WHERE MaSP = '$id'";
        Yii::app()->db->createCommand($sql)->execute();
        header("location: ..");
    }
	
	 /**
     * Edit
     */
    public function actionEdit() {
        //Yii::log('Edit', CLogger::LEVEL_INFO, 'yiilog');
        
        $id = $_GET['id'];
        $bookLib = new ProductList();
        $row = $bookLib->getPrdbyId($id);        

        //$sql = "DELETE FROM books WHERE id = '$id'";
        //Yii::app()->db->createCommand($sql)->execute();

        if (isset($_POST['update'])) {            
            //$sql = "UPDATE books ";
        }

        //echo "ddd";
        
          

    }
	
	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Cam on vi da gui thong tin lien he cho chung toi.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}
	
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
        Cookie::set('username', null);
        Yii::app()->user->setId(null);
        Yii::app()->user->setName('guest');
        $this->redirect(Yii::app()->homeUrl);
	}

}
